import java.util.Scanner;

public class Bai4 {
    public static void main(String[] args) {
        double a,b,x;
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập số a");
        a = sc.nextDouble();
        System.out.println("Nhập số b");
        b = sc.nextDouble();

        if(a == 0) {
            if (a == b) {
                System.out.println("Phương trình có vô số nghiệm");
            } else {
                System.out.println("Phương trình vô nghiệm");
            }
        }
        if ( a != 0) {
            if (b == 0) {
                System.out.println("Phương trình có nghiệm x = 0");
            } else {
                System.out.println("Phương trình có nghiệm x = " + (-b/a));
            }
        }
    }
}
