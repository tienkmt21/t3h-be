import java.util.Scanner;

public class Bai10 {
    public static void main(String[] args) {
        int number;
        int count = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập số có 4 chữ số : ");
        number = sc.nextInt();

        if (number < 1000 || number > 9999) {
            System.out.print("Nhập sai");
            return;
        }
        while (count <= 1) {
            if (count == 1) {
                System.out.print("Số hàng chục là " + number%10);
            }
            number /= 10;
            count++;
        }
    }
}
