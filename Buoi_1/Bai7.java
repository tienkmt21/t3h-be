import java.util.Scanner;

public class Bai7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int first,second,third;
        System.out.println("Nhập số thứ nhất");
        first = sc.nextInt();
        System.out.println("Nhập số thứ hai");
        second = sc.nextInt();
        System.out.println("Nhập số thứ ba");
        third = sc.nextInt();

        if (first%2 == 1) {
            System.out.println(first + " là số lẻ");
        } else {
            System.out.println(first + " là số chẵn");
        }

        if (second%2 == 1) {
            System.out.println(second + " là số lẻ");
        } else {
            System.out.println(second + " là số chẵn");
        }

        if (third%2 == 1) {
            System.out.println(third + " là số lẻ");
        } else {
            System.out.println(third + " là số chẵn");
        }
    }
}
