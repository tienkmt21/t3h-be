import java.util.Scanner;

public class Bai11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double first,second;
        System.out.println("Nhập số thứ nhất");
        first = sc.nextDouble();
        System.out.println("Nhập số thứ hai");
        second = sc.nextDouble();

        String[] Sfirst = String.valueOf(first).split("\\.");
        String[] Ssecond = String.valueOf(second).split("\\.");

        System.out.println("Phần thập phân số thứ nhất " + Sfirst[1]);
        System.out.println("Phần thập phân số thứ hai " + Ssecond[1]);
    }
}
