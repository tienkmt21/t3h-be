public class Human {
    private String name;
    private int age;
    private String address;
    private long number;

    private static Human huMan = new Human();

    private void walk() {
        System.out.println("i'm walking");
    }

    private void eat() {
        System.out.println("i'm eating");
    }

    public static void main(String[] args) {
        huMan.walk();
        huMan.eat();
    }

}
