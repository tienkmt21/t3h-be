import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double a,b,c,delta,x1,x2;
        System.out.println("Nhập số a");
        a = sc.nextDouble();
        System.out.println("Nhập số b");
        b = sc.nextDouble();
        System.out.println("Nhập số c");
        c = sc.nextDouble();

        delta = b*b-4*a*c;
        if (delta < 0) {
            System.out.println("Phương trình vô nghiệm");
        } else if (delta == 0) {
            System.out.println("Phương trình có 1 nghiệm kép" + (-b/(2*a)));
        } else {
            x1 = (-b + Math.sqrt(delta))/(2*a);
            x2 = (-b - Math.sqrt(delta))/(2*a);
            System.out.println(String.format("Phương trình có 2 nghiệm x1 = %s x2 = %s",x1,x2));
        }
    }
}
