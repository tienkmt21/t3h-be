import java.util.Scanner;

public class Bai8 {
    public static void main(String[] args) {
        int number;
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập số nguyên : ");
        number = sc.nextInt();

        for (int i = 2; i <= number-1; i++) {
            if (number % i == 0) {
                System.out.print("Số này không phải số nguyên tố.");
                break;
            }
            if (i == number-1) {
                System.out.print("Số này là số nguyên tố.");
            }
        }
    }
}
